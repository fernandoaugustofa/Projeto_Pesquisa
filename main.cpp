#include <iostream>
#include <cstdlib>

#include <opencv2/dnn.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include "opencv2/core.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/xfeatures2d.hpp"

using namespace std;
using namespace cv;
using namespace cv::dnn;
using namespace cv::xfeatures2d;


const size_t inWidth = 300;
const size_t inHeight = 300;
const int Altura = 480;
const int Largura = 720;
const double inScaleFactor = 1.0;
const Scalar meanVal(104.0, 177.0, 123.0);
int minHessian = 1;

int main()
{
    int flagCameraOuArquivo = 1;
    int cameraDevice = 0;
    int Altura_linha = 300;
    cv::Point Last_point;
    string nomeArquivo = "/home/andromeda/Desktop/Projeto_Pesquisa/Teste10.mp4";
    float confidenceThreshold = 0.2;

    //! [Initialize network]

   dnn::Net net = readNetFromCaffe("/home/andromeda/Desktop/Projeto_Pesquisa/deploy.prototxt.txt", "/home/andromeda/Desktop/Projeto_Pesquisa/res10_300x300_ssd_iter_140000.caffemodel");

    //! [Initialize network]

    VideoCapture cap;
    if (flagCameraOuArquivo == 1)
    {
        cap = VideoCapture(cameraDevice);
        if(!cap.isOpened())
        {
            cout << "Couldn't find camera: " << cameraDevice << endl;
            return -1;
        }
    }
    else
    {
        cap.open(nomeArquivo);
        if(!cap.isOpened())
        {
            cout << "Couldn't open image or video: " << nomeArquivo << endl;
            return -1;
        }
    }

    Mat frame_trajectory = Mat(Altura,Largura, CV_8UC3,Scalar(255, 255, 255));
    int iii = 0;
    Mat images;
    for(;;)
    {
        Mat frame;
        Mat Clean_frame;
        cap >> frame;

        if (frame.empty())
        {
            waitKey();
            break;
        }

        cv::resize(frame, frame, Size(Largura, Altura));
        frame.copyTo(Clean_frame);
        if (frame.channels() == 4)
            cvtColor(frame, frame, COLOR_BGRA2BGR);

        //! [Prepare blob]
        Mat inputBlob = blobFromImage(frame, inScaleFactor,
                                      Size(inWidth, inHeight), meanVal, false, false); //Convert Mat to batch of images
        //! [Prepare blob]

        //! [Set input blob]
        net.setInput(inputBlob, "data"); //set the network input
        //! [Set input blob]

        //! [Make forward pass]
        Mat detection = net.forward("detection_out"); //compute output
        //! [Make forward pass]

        vector<double> layersTimings;
        double freq = getTickFrequency() / 1000;
        double time = net.getPerfProfile(layersTimings) / freq;

        Mat detectionMat(detection.size[2], detection.size[3], CV_32F, detection.ptr<float>());
        Mat crop;
        ostringstream ss;
        ss << "FPS: " << 1000/time << " ; time: " << time << " ms";
        putText(frame, ss.str(), Point(20,20), 0, 0.5, Scalar(0,0,255));

        for(int i = 0; i < detectionMat.rows; i++)
        {
            float confidence = detectionMat.at<float>(i, 2);

            if(confidence > confidenceThreshold)
            {
                Ptr<SURF> detector = SURF::create();
                detector->setHessianThreshold(minHessian);
                std::vector<KeyPoint> keypoints_1, keypoints_2;
                Mat descriptors_1, descriptors_2;
                int xLeftBottom = static_cast<int>(detectionMat.at<float>(i, 3) * frame.cols);
                int yLeftBottom = static_cast<int>(detectionMat.at<float>(i, 4) * frame.rows);
                int xRightTop = static_cast<int>(detectionMat.at<float>(i, 5) * frame.cols);
                int yRightTop = static_cast<int>(detectionMat.at<float>(i, 6) * frame.rows);

                Rect object((int)xLeftBottom, (int)yLeftBottom,
                            (int)(xRightTop - xLeftBottom),
                            (int)(yRightTop - yLeftBottom));

                ss.str("");
                ss << confidence;
                String conf(ss.str());
                String label = "Face: " + conf;
                int baseLine = 0;
                circle(frame_trajectory,Last_point,1,Scalar(255,0,255));
                circle(frame,Last_point,4,Scalar(255,0,255));
                circle(frame,Point(0,Last_point.y),4,Scalar(0,0,255));
                Size labelSize = getTextSize(label, FONT_HERSHEY_SIMPLEX, 0.5, 1, &baseLine);
                rectangle(frame, Rect(Point(xLeftBottom, yLeftBottom - labelSize.height),
                                      Size(labelSize.width, labelSize.height + baseLine)),
                          Scalar(255, 255, 255), CV_FILLED);
                putText(frame, label, Point(xLeftBottom, yLeftBottom),
                        FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0,0,0));
                Rect New_object;
                New_object.x = object.x - 50;
                New_object.y = object.y - 50;
                New_object.height = object.height + 100;
                New_object.width = object.width + 100;
                if(iii >= 1) {
                    try {
                        Mat crop0 = images;
                        cvtColor(crop0, crop0, COLOR_BGR2GRAY);
                        Mat crop2 = Clean_frame(New_object);
                        cvtColor(crop2, crop2, COLOR_BGR2GRAY);
                        detector->detectAndCompute(crop0, Mat(), keypoints_1, descriptors_1);
                        detector->detectAndCompute(crop2, Mat(), keypoints_2, descriptors_2);
                        FlannBasedMatcher matcher;
                        std::vector<DMatch> matches;
                        matcher.match(descriptors_1, descriptors_2, matches);
                        double max_dist = 0;
                        double min_dist = 100;
                        for (int i = 0; i < descriptors_1.rows; i++) {
                            double dist = matches[i].distance;
                            if (dist < min_dist) min_dist = dist;
                            if (dist > max_dist) max_dist = dist;
                        }
                        std::vector<DMatch> good_matches;
                        for (int i = 0; i < descriptors_1.rows; i++) {
                            if (matches[i].distance <= max(2 * min_dist, 0.02)) { good_matches.push_back(matches[i]); }
                        }
                        //-- Draw only "good" matches
                        Mat img_matches;
                        drawMatches(crop0, keypoints_1, crop2, keypoints_2,
                                    good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
                                    vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
                        //-- Show detected matches
                        imshow("Good Matches", img_matches);
                    } catch (Exception e) {
                        cout << ":(" << endl;
                    }
                }
                Last_point.x = (xRightTop + xLeftBottom)/2;
                Last_point.y = (yRightTop + yLeftBottom)/2;
//                cout << crop.rows << endl;
                rectangle(frame, object, Scalar(255, 255, 0));
//                if (Last_point.y >= Altura_linha + 3 || Last_point.y <= Altura_linha - 3){
//                    line(frame, Point(0, Altura_linha), Point(Largura, Altura_linha), Scalar(50, 255, 50), 6);
//                }else {
//                    line(frame, Point(0, Altura_linha), Point(Largura, Altura_linha), Scalar(50, 50, 255), 6);
                    try {
                        crop = Clean_frame(New_object);
                        cout <<iii << endl;
                        if(iii % 60 == 1) {
                            images = crop;
                        }
                        iii++;

                    }catch (Exception e) {
                        cout << "Não Foi possivel capturar este Rosto. Desculpe :(" << endl;
                    }
//                }

            }
        }

        imshow("detections", frame);
        imshow("trajectory", frame_trajectory);
      //  imshow("leave", images[0]);
       // imshow("Clean", Clean_frame);
        if (waitKey(1) >= 0) break;
    }

    return 0;
} // main