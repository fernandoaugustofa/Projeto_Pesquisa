#!/bin/bash

# Definição de variáveis.
versaoOpenCV3="3.4.0";
arquivoOpenCV3="${versaoOpenCV3}.zip"
pastaOpenCV3="opencv-${versaoOpenCV3}"
urlDownloadOpenCV3="https://github.com/opencv/opencv/archive/${arquivoOpenCV3}"
numCPUs=`grep -c ^processor /proc/cpuinfo`

# Baixa bibliotecas de sistema.
sudo apt install -y build-essential cmake git pkg-config unzip ffmpeg qtbase5-dev python-dev python3-dev python-numpy python3-numpy libopencv-dev libgtk-3-dev libdc1394-22 libdc1394-22-dev libjpeg-dev libpng12-dev libtiff5-dev libjasper-dev libavcodec-dev libavformat-dev libswscale-dev libxine2-dev libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev libv4l-dev libtbb-dev libfaac-dev libmp3lame-dev libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev libvorbis-dev libxvidcore-dev v4l-utils vtk6 liblapacke-dev libopenblas-dev libgdal-dev checkinstall

# Verifica se o arquivo do código-fonte do OpenCV já foi baixado. Se não foi, baixa.
if [ -e "$arquivoOpenCV3" ]; then
	echo "Arquivo OpenCV3 ja existente e nao sera feito novo download!"
else
	wget $urlDownloadOpenCV3
fi;

# Apaga a pasta do código-fonte do OpenCV, se existir, e o compila usando o máximo de CPUs que existe no sistema.
if [ -e "$pastaOpenCV3" ]; then
	rm -rf "$pastaOpenCV3"
fi
unzip "$arquivoOpenCV3"
cd "$pastaOpenCV3"
mkdir build
cd build
cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local ..
make -j"$numCPUs"

# Instala o OpenCV no sistema.
sudo make install
sudo /bin/bash -c 'echo "/usr/local/lib" > /etc/ld.so.conf.d/opencv.conf'
sudo ldconfig
sudo apt-get update
